class SearchSample {
  String? id;
  String? title;
  String? image;
  String? type;

  SearchSample({this.id, this.title, this.image, this.type});

  SearchSample.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    image = json['image'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['image'] = this.image;
    data['type'] = this.type;
    return data;
  }
}
