import '../models/AnimesModel.dart';

abstract class GetMovieAnimesRepositry {
  Future<List<Animes>> getAnimes({String? genre, required bool isGenre});
}
