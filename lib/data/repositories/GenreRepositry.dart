abstract class GenreRepositry {
  Future<List<String>> getAnimeGenere();
}
