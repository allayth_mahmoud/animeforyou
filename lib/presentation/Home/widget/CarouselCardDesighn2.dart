import 'package:flutter/material.dart';

import '../../../core/Theme.dart';
import '../../../core/settings.dart';
import '../../../data/models/AnimesModel.dart';
import '../../AnimeDieatels/AnimeDiatels.dart';

class CarouselCardDesighn2 extends StatelessWidget {
  const CarouselCardDesighn2({
    super.key,
    required this.anime,
  });
  final Animes anime;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: SizedBox(
        height: media(context).height * 0.3,
        width: media(context).width * 0.9,
        child: Card(
          color: Colors.transparent,
          clipBehavior: Clip.hardEdge,
          margin: const EdgeInsets.all(0),
          child: Stack(clipBehavior: Clip.hardEdge, children: [
            Positioned.fill(
                child: Image.network(
              errorBuilder: (context, error, stackTrace) =>
                  const Center(child: Icon(Icons.error, color: Colors.red)),
              fit: BoxFit.cover,
              anime.image!,
            )),
            Positioned(
              bottom: 0,
              child: Container(
                alignment: Alignment.center,
                width: media(context).width * 0.41,
                height: media(context).height * 0.28,
                padding: const EdgeInsets.symmetric(horizontal: 3),
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.centerRight,
                        end: Alignment.centerLeft,
                        colors: [
                      Colors.transparent,
                      Colors.grey[50]!.withOpacity(0.6),
                      Colors.grey[100]!.withOpacity(0.6),
                      Colors.grey[200]!.withOpacity(0.6),
                      Colors.grey[300]!.withOpacity(0.6),
                      Colors.grey[350]!.withOpacity(0.6),
                    ])),
                child: Text(
                  maxLines: 3,
                  anime.title!,
                  style: textStyle(
                      color: Colors.grey[900]!,
                      size: media(context).width * 0.05,
                      weight: FontWeight.bold),
                ),
              ),
            ),
            Positioned(
                right: 10,
                bottom: 2,
                child: InkWell(
                  onTap: () => Navigator.of(context)
                      .pushNamed(AnimeDetiels.routeName, arguments: anime),
                  child: Container(
                      width: media(context).width * 0.42,
                      height: media(context).height * 0.05,
                      decoration: BoxDecoration(
                          color: Colors.amber[400]!.withOpacity(0.8),
                          borderRadius: BorderRadius.circular(5)),
                      margin: const EdgeInsets.symmetric(vertical: 5),
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const Expanded(
                                flex: 1,
                                child: Icon(
                                  Icons.settings_ethernet_sharp,
                                  size: 25,
                                  color: Colors.black,
                                )),
                            Expanded(
                              flex: 2,
                              child: FittedBox(
                                child: Text(
                                  maxLines: 1,
                                  ' See Dieatels',
                                  style: textStyle(
                                      size: 15,
                                      color: Colors.black,
                                      weight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ])),
                ))
          ]),
        ),
      ),
    );
  }
}
