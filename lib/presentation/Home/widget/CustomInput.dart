import 'package:flutter/material.dart';

import '../../../core/settings.dart';
import '../../SearchPage/SearchPage.dart';

class CustomInput extends StatelessWidget {
  CustomInput({
    super.key,
  });
  TextEditingController controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: media(context).width * 0.8,
      height: media(context).height * 0.08,
      child: TextFormField(
        controller: controller,
        style: TextStyle(color: Colors.grey[900]),
        textAlignVertical: TextAlignVertical.center,
        decoration: InputDecoration(
          hintStyle: TextStyle(color: Colors.grey[800], fontSize: 15),
          fillColor: Colors.grey.shade300,
          filled: true,
          suffixIcon: IconButton(
            icon: const Icon(Icons.search),
            onPressed: () {
              Navigator.of(context).pushNamed(SearchView.routeName,
                  arguments: controller.text.trim());
            },
          ),
          hintText: 'Type in your search',
          border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey[300]!)),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey[300]!)),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey[300]!)),
        ),
      ),
    );
  }
}
