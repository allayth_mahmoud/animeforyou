import 'package:flutter/material.dart';

import '../../../core/Theme.dart';
import '../../../core/settings.dart';
import '../../../data/models/AnimesModel.dart';
import '../../AnimeDieatels/AnimeDiatels.dart';

class CarouselCard extends StatelessWidget {
  const CarouselCard(
      {super.key, required this.image, required this.txt, required this.anime});

  final String image;
  final String txt;
  final Animes anime;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context)
            .pushNamed(AnimeDetiels.routeName, arguments: anime);
      },
      child: SizedBox(
        height: media(context).height * 0.5,
        child: Column(
          children: [
            Expanded(
              flex: 4,
              child: Card(
                color: Colors.transparent,
                clipBehavior: Clip.hardEdge,
                margin: const EdgeInsets.all(0),
                child: Stack(clipBehavior: Clip.hardEdge, children: [
                  Positioned.fill(
                      child: Image.network(
                    errorBuilder: (context, error, stackTrace) => const Center(
                        child: Icon(Icons.error, color: Colors.red)),
                    fit: BoxFit.fill,
                    image,
                  )),
                ]),
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(
                maxLines: 2,
                txt,
                style: textStyle(
                    color: Colors.white, size: 16, weight: FontWeight.normal),
              ),
            )
          ],
        ),
      ),
    );
  }
}
