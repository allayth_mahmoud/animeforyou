import 'package:flutter/material.dart';

import '../../../core/Theme.dart';

class SpanCustomText extends StatelessWidget {
  const SpanCustomText({
    super.key,
    required this.title,
    required this.body,
    required this.sizeT,
    required this.sizeB,
  });

  final String title;
  final String body;
  final double sizeT;
  final double sizeB;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: FittedBox(
        child: RichText(
          text: TextSpan(children: [
            TextSpan(
              text: '$title :   ',
              style: textStyle(
                  color: Colors.white, size: sizeT, weight: FontWeight.bold),
            ),
            TextSpan(
              text: body,
              style: textStyle(
                  color: Colors.amber[600]!,
                  size: sizeB,
                  weight: FontWeight.bold),
            ),
          ]),
        ),
      ),
    );
  }
}
