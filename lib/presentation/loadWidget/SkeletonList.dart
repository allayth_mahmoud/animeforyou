import 'package:flutter/material.dart';

import '../../core/loadingEffict.dart';

class SkeletonList extends StatelessWidget {
  const SkeletonList({
    super.key,
    required this.mediaQuery,
  });
  final Size mediaQuery;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        padding: const EdgeInsets.all(3),
        itemCount: 10,
        itemExtent: 80,
        itemBuilder: (context, index) {
          return Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Skeleton.circular(width: 70, height: 80),
                SizedBox(
                  height: mediaQuery.height * 0.06,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Skeleton.rectangular(
                          width: mediaQuery.width * 0.5, height: 10),
                      Skeleton.rectangular(
                          width: mediaQuery.width * 0.68, height: 10),
                      Skeleton.rectangular(
                          width: mediaQuery.width * 0.75, height: 10)
                    ],
                  ),
                )
              ]);
        });
  }
}
