import 'package:flutter/material.dart';

import '../../core/loadingEffict.dart';

class SkeletonProfile extends StatelessWidget {
  const SkeletonProfile({
    super.key,
    required this.mediaQuery,
  });

  final Size mediaQuery;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Skeleton.circular(
                  width: mediaQuery.width * 0.4,
                  height: mediaQuery.height * 0.2),
              SizedBox(
                height: mediaQuery.height * 0.15,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Skeleton.rectangular(
                        width: mediaQuery.width * 0.32, height: 12),
                    Skeleton.rectangular(
                        width: mediaQuery.width * 0.38, height: 12),
                    Skeleton.rectangular(
                        width: mediaQuery.width * 0.46, height: 11),
                    SizedBox(
                      height: mediaQuery.height * 0.03,
                    )
                  ],
                ),
              )
            ],
          ),
          SizedBox(
            height: mediaQuery.height * 0.63,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Skeleton.rectangular(width: mediaQuery.width * 0.9, height: 5),
                Skeleton.rectangular(width: mediaQuery.width * 0.9, height: 50),
                Skeleton.rectangular(width: mediaQuery.width * 0.9, height: 45),
                Skeleton.rectangular(width: mediaQuery.width * 0.9, height: 40),
                Skeleton.rectangular(width: mediaQuery.width * 0.9, height: 30),
                Skeleton.rectangular(width: mediaQuery.width * 0.9, height: 20),
                Skeleton.rectangular(width: mediaQuery.width * 0.9, height: 10),
                Skeleton.rectangular(width: mediaQuery.width * 0.9, height: 10),
                Skeleton.rectangular(width: mediaQuery.width * 0.9, height: 10),
                Skeleton.rectangular(width: mediaQuery.width * 0.9, height: 10),
                Skeleton.rectangular(width: mediaQuery.width * 0.9, height: 10),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
