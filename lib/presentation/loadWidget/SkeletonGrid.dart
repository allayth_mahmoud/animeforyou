import 'package:flutter/material.dart';

import '../../core/loadingEffict.dart';

class SkeletonGrid extends StatelessWidget {
  const SkeletonGrid({
    super.key,
    required this.mediaQuery,
  });
  final Size mediaQuery;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: 15,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisSpacing: 5, mainAxisSpacing: 5, crossAxisCount: 2),
        itemBuilder: (context, index) =>
            Column(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
              Skeleton.rectangular(width: 150, height: 70),
              SizedBox(
                height: mediaQuery.height * 0.05,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Skeleton.rectangular(
                        width: mediaQuery.width * 0.15, height: 7),
                    Skeleton.rectangular(
                        width: mediaQuery.width * 0.2, height: 7),
                    Skeleton.rectangular(
                        width: mediaQuery.width * 0.25, height: 7)
                  ],
                ),
              )
            ]));
  }
}
