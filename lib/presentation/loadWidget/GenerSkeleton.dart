import 'package:flutter/cupertino.dart';

import '../../core/loadingEffict.dart';
import '../../core/settings.dart';

class GenerSkeleton extends StatelessWidget {
  const GenerSkeleton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: media(context).width,
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
        Skeleton.rectangular(
          width: media(context).width * 0.2,
          height: media(context).height * 0.04,
        ),
        Skeleton.rectangular(
          width: media(context).width * 0.2,
          height: media(context).height * 0.04,
        ),
        Skeleton.rectangular(
          width: media(context).width * 0.2,
          height: media(context).height * 0.04,
        ),
        Skeleton.rectangular(
          width: media(context).width * 0.2,
          height: media(context).height * 0.04,
        )
      ]),
    );
  }
}
