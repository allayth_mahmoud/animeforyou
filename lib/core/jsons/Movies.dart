

Map movies = {
  "data": [
    {
      "_id": "38061",
      "title": "One Room: Second Season - Hanasaka Yui no Prologue",
      "alternativeTitles": [
        "One Room 2nd Season Episode 0",
        "One Room 2nd Season: Hanasaka Yui no Prologue",
        "One Room セカンドシーズン　花坂結衣のプロローグ",
        "One Room 2nd Season: Hanasaka Yui's Prologue",
        "One Room Staffel 2: Folge 0 Hanasaki Yuis Prolog",
        "One Room Temporada 2: Prólogo de Yui",
        "One Room Saison 2: Le prologue de Yui Hanasaka"
      ],
      "ranking": 9764,
      "genres": [],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1473/93256.webp",
      "link":
          "https://myanimelist.net/anime/38061/One_Room__Second_Season_-_Hanasaka_Yui_no_Prologue",
      "status": "Finished Airing",
      "synopsis":
          "A summary of One Room's first 4 episodes aired as a prequel to the second season.",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1473/93256.webp?s=fe999492e0890fb7f37e830822daa61b",
      "type": "Special"
    },
    {
      "_id": "459",
      "title": "One Piece Movie 01",
      "alternativeTitles": [
        "One Piece: The Movie",
        "ONE PIECE",
        "One Piece: The Movie",
        "One Piece: Der Film",
        "One Piece: La Película",
        "One Piece: Le Film"
      ],
      "ranking": 3583,
      "genres": ["Action", "Adventure", "Fantasy"],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1770/97704.webp",
      "link": "https://myanimelist.net/anime/459/One_Piece_Movie_01",
      "status": "Finished Airing",
      "synopsis":
          "Many years ago, Woonan, a legendary pirate, plundered one-third of the world's gold and stashed it away on his secret island shrouded in mystery. \n\nIn the present, Luffy and the rest of the Straw Hats continue on their journey to the Grand Line. They are robbed by a group of bandits. Led by their captain, El Drago, these same bandits are headed towards Woonan's famed island. Even though the Straw Hat Pirates must now recover their stolen treasure, they have gained another objective: to discover the lost island, its treasures, and learn about the legend of Woonan.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1770/97704.webp?s=9534ff79db4c96f1ae9bea78a141c532",
      "type": "Movie"
    },
    {
      "_id": "34134",
      "title": "One Punch Man 2nd Season",
      "alternativeTitles": [
        "One Punch-Man 2",
        "One-Punch Man 2",
        "OPM 2",
        "ワンパンマン",
        "One Punch Man Season 2",
        "One Punch Man Staffel 2",
        "One Punch Man Temporada 2",
        "One Punch Man Saison 2"
      ],
      "ranking": 1722,
      "genres": ["Action", "Comedy"],
      "episodes": 12,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1247/122044.webp",
      "link": "https://myanimelist.net/anime/34134/One_Punch_Man_2nd_Season",
      "status": "Finished Airing",
      "synopsis":
          "In the wake of defeating Boros and his mighty army, Saitama has returned to his unremarkable everyday life in Z-City. However, unbeknownst to him, the number of monsters appearing is still continuously on the rise, putting a strain on the Hero Association's resources. Their top executives decide on the bold move of recruiting hoodlums in order to help in their battle. But during the first meeting with these potential newcomers, a mysterious man calling himself Garou makes his appearance. Claiming to be a monster, he starts mercilessly attacking the crowd.\n\nThe mysterious Garou continues his rampage against the Hero Association, crushing every hero he encounters. He turns out to be the legendary martial artist Silverfang's best former disciple and seems driven by unknown motives. Regardless, this beast of a man seems unstoppable. Intrigued by this puzzling new foe and with an insatiable thirst for money, Saitama decides to seize the opportunity and joins the interesting martial arts competition.\n\nAs the tournament commences and Garou continues his rampage, a new great menace reveals itself, threatening the entire human world. Could this finally be the earth-shattering catastrophe predicted by the great seer Madame Shibabawa?\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1247/122044.webp?s=6b257dab3772cad91a374f3162abc107",
      "type": "TV"
    },
    {
      "_id": "39652",
      "title": "One Punch Man 2nd Season Commemorative Special",
      "alternativeTitles": [
        "One Punch-Man 2",
        "One-Punch Man 2",
        "OPM 2",
        "ワンパンマン2ndシーズン記念スペシャル",
        "One Punch Man 2nd Season Commemorative Special",
        "One Punch Man Staffel 2 Folge 0",
        "One Punch Man Temporada 2 Episodio 0",
        "One Punch Man Saison 2 Épisode 0"
      ],
      "ranking": 4061,
      "genres": ["Action", "Comedy"],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1550/123539.webp",
      "link":
          "https://myanimelist.net/anime/39652/One_Punch_Man_2nd_Season_Commemorative_Special",
      "status": "Finished Airing",
      "synopsis": "One Punch Man 2nd Season's commemorative recap special.",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1550/123539.webp?s=5d4c64ffdc975095bc495e8bfc2aa4f3",
      "type": "Special"
    },
    {
      "_id": "460",
      "title": "One Piece Movie 02: Nejimaki-jima no Daibouken",
      "alternativeTitles": [
        "One Piece: Nejimakijima no Bouken",
        "One Piece: Nejimaki Shima no Bouken",
        "ワンピース ねじまき島の冒険",
        "One Piece: Clockwork Island Adventure",
        "One Piece Film 2: Abenteuer auf der Spiralinsel!",
        "One Piece Película 2: Aventura en la Isla Espiral",
        "One Piece Film 2: L'Aventure de L'île de L'horloge"
      ],
      "ranking": 3524,
      "genres": ["Action", "Adventure", "Fantasy"],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1939/97699.webp",
      "link":
          "https://myanimelist.net/anime/460/One_Piece_Movie_02__Nejimaki-jima_no_Daibouken",
      "status": "Finished Airing",
      "synopsis":
          "Informed by the Thief Brothers his ship has been stolen by the Trump Kyoudai (Trump Siblings) who have set up base on Clockwork Island. Monkey D. Luffy, Captain of the Going Merry and aspiring Pirate King works with his crew - Usopp, Zoro, Sanji and Nami to battle their way up Clockwork Island to reclaim their ship.\n\n(Source: ANN)",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1939/97699.webp?s=f44dd050c8fc984b9320021ce4384fdb",
      "type": "Movie"
    },
    {
      "_id": "465",
      "title": "One Piece Movie 07: Karakuri-jou no Mecha Kyohei",
      "alternativeTitles": [
        "One Piece The Movie: Karakurijou no Mecha Kyohei",
        "One Piece: Karakuri Shiro no Mecha Kyohei",
        "Karakuri Castle's Mecha Giant Soldier",
        "ワンピース THE MOVIE カラクリ城のメカ巨兵",
        "One Piece: The Giant Mechanical Soldier of Karakuri Castle",
        "One Piece Film 7: Schloss Karakuris Metall-Soldaten",
        "One Piece Película 7: El Gran Soldado Mecánico del Castillo Karakuri",
        "One Piece Film 7: Le Mecha Géant du Château Karakuri"
      ],
      "ranking": 3049,
      "genres": ["Action", "Adventure", "Fantasy"],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/9/20927.webp",
      "link":
          "https://myanimelist.net/anime/465/One_Piece_Movie_07__Karakuri-jou_no_Mecha_Kyohei",
      "status": "Finished Airing",
      "synopsis":
          "The Straw Hat crew salvages a treasure chest from a sinking wreck, inside which they find an old lady. To get the pirates to take her home, she promises them the treasure of a golden crown on her island: Mecha Island. Upon arrival, the crew is attacked by the lord of the island but he has a change of heart and decides to seek their help to solve the mystery of the Golden Crown.\n\n(Source: ANN, edited)",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/9/20927.webp?s=107e2476ec3f92aac06233a4b3be55e5",
      "type": "Movie"
    },
    {
      "_id": "36431",
      "title": "One Room: Second Season",
      "alternativeTitles": [
        "One Room 2nd Season",
        "One Room セカンドシーズン",
        "One Room Zweite Staffel",
        "One Room Temporada 2",
        "One Room Saison 2"
      ],
      "ranking": 8434,
      "genres": [],
      "episodes": 12,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1098/93257.webp",
      "link": "https://myanimelist.net/anime/36431/One_Room__Second_Season",
      "status": "Finished Airing",
      "synopsis": "Second season of One Room. ",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1098/93257.webp?s=0bbe509193a371a06a3c4f8a2ead11f3",
      "type": "TV"
    },
    {
      "_id": "41364",
      "title": "One Room: Third Season",
      "alternativeTitles": [
        "One Room 3rd Season",
        "One Room サードシーズン",
        "One Room Staffel 3",
        "One Room Temporada 3",
        "One Room Saison 3"
      ],
      "ranking": 6649,
      "genres": [],
      "episodes": 12,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1553/109101.webp",
      "link": "https://myanimelist.net/anime/41364/One_Room__Third_Season",
      "status": "Finished Airing",
      "synopsis": "Third season of One Room. ",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1553/109101.webp?s=17a202735cab9d611aef66e6b5042cd1",
      "type": "TV"
    },
    {
      "_id": "31490",
      "title": "One Piece Film: Gold",
      "alternativeTitles": [
        "One Piece Movie 13",
        "ONE PIECE FILM GOLD",
        "One Piece Film 13: Gold",
        "One Piece Película 13: Gold",
        "One Piece Film 13: Gold"
      ],
      "ranking": 724,
      "genres": ["Action", "Adventure", "Fantasy"],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/12/81081.webp",
      "link": "https://myanimelist.net/anime/31490/One_Piece_Film__Gold",
      "status": "Finished Airing",
      "synopsis":
          "Monkey D. Luffy and his Straw Hat Crew have finally arrived on Gran Tesoro, a ship carrying the largest entertainment city in the world. Drawn in by the chances of hitting the jackpot, the crew immediately head to the casino. There, they quickly find themselves on a winning streak, playing with what seems to be endless luck.\n\nWhen offered a special gamble by Gild Tesoro—the master of the city himself—the crew agrees, choosing to believe in their captain's luck. However, when they find themselves victims of a despicable scam, the crew quickly realize that there is something darker happening beneath the city's surface.\n\nLeft penniless and beaten down, the Straw Hat Crew are forced to rely on another gamble of a plan. With the help of a new friend or two, the group must work to reclaim what they've lost before time, and what remains of their luck, runs out.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/12/81081.webp?s=5b0c55c3c60442ff76276c85e5fca610",
      "type": "Movie"
    },
    {
      "_id": "38234",
      "title": "One Piece Movie 14: Stampede",
      "alternativeTitles": [
        "劇場版『ONE PIECE STAMPEDE』（スタンピード）",
        "One Piece: Stampede",
        "One Piece Film 14: Stampede",
        "One Piece Película 14: Estampida",
        "One Piece Film 14: Stampede"
      ],
      "ranking": 325,
      "genres": ["Action", "Adventure", "Fantasy"],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1221/100550.webp",
      "link":
          "https://myanimelist.net/anime/38234/One_Piece_Movie_14__Stampede",
      "status": "Finished Airing",
      "synopsis":
          "Monkey D. Luffy and the Straw Hats arrive aboard the Sunny to the Pirates Festival, the world's largest celebration created by and for pirates. Buena Festa, the festival organizer, invites the Straw Hats and all Worst Generation crews to partake in the festivities. Luring even Shichibukai and Marines to its shores, it seems that no pirate or sailor can resist the enticing secrets that the event hides behind its glamor.\n\nThe festival's contest is simple: find one of the treasures Gol D. Roger left behind. As the competition progresses, the various pirate crews fight each other in a free-for-all battle royale—that is, until the sudden appearance of an unexpected pirate drastically changes the game.\n\n[Written by MAL Rewrite]\n",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1221/100550.webp?s=434da4b5a965d310c13f72832eebe7c4",
      "type": "Movie"
    },
    {
      "_id": "5114",
      "title": "Fullmetal Alchemist: Brotherhood",
      "alternativeTitles": [
        "Hagane no Renkinjutsushi: Fullmetal Alchemist",
        "Fullmetal Alchemist (2009)",
        "FMA",
        "FMAB",
        "鋼の錬金術師 FULLMETAL ALCHEMIST",
        "Fullmetal Alchemist: Brotherhood",
        "Fullmetal Alchemist Brotherhood"
      ],
      "ranking": 1,
      "genres": ["Action", "Adventure", "Drama", "Fantasy"],
      "episodes": 64,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1208/94745.webp",
      "link":
          "https://myanimelist.net/anime/5114/Fullmetal_Alchemist__Brotherhood",
      "status": "Finished Airing",
      "synopsis":
          "After a horrific alchemy experiment goes wrong in the Elric household, brothers Edward and Alphonse are left in a catastrophic new reality. Ignoring the alchemical principle banning human transmutation, the boys attempted to bring their recently deceased mother back to life. Instead, they suffered brutal personal loss: Alphonse's body disintegrated while Edward lost a leg and then sacrificed an arm to keep Alphonse's soul in the physical realm by binding it to a hulking suit of armor.\n\nThe brothers are rescued by their neighbor Pinako Rockbell and her granddaughter Winry. Known as a bio-mechanical engineering prodigy, Winry creates prosthetic limbs for Edward by utilizing \"automail,\" a tough, versatile metal used in robots and combat armor. After years of training, the Elric brothers set off on a quest to restore their bodies by locating the Philosopher's Stone—a powerful gem that allows an alchemist to defy the traditional laws of Equivalent Exchange.\n\nAs Edward becomes an infamous alchemist and gains the nickname \"Fullmetal,\" the boys' journey embroils them in a growing conspiracy that threatens the fate of the world.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1208/94745.webp?s=f286786e3bc43d6dc5b4478a1762224b",
      "type": "TV"
    },
    {
      "_id": "41467",
      "title": "Bleach: Sennen Kessen-hen",
      "alternativeTitles": [
        "Bleach: Thousand-Year Blood War Arc",
        "BLEACH 千年血戦篇",
        "Bleach: Thousand-Year Blood War"
      ],
      "ranking": 2,
      "genres": ["Action", "Adventure", "Fantasy"],
      "episodes": 13,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1956/126621.webp",
      "link": "https://myanimelist.net/anime/41467/Bleach__Sennen_Kessen-hen",
      "status": "Finished Airing",
      "synopsis":
          "Substitute Soul Reaper Ichigo Kurosaki spends his days fighting against Hollows, dangerous evil spirits that threaten Karakura Town. Ichigo carries out his quest with his closest allies: Orihime Inoue, his childhood friend with a talent for healing; Yasutora Sado, his high school classmate with superhuman strength; and Uryuu Ishida, Ichigo's Quincy rival.\n\nIchigo's vigilante routine is disrupted by the sudden appearance of Asguiaro Ebern, a dangerous Arrancar who heralds the return of Yhwach, an ancient Quincy king. Yhwach seeks to reignite the historic blood feud between Soul Reaper and Quincy, and he sets his sights on erasing both the human world and the Soul Society for good.\n\nYhwach launches a two-pronged invasion into both the Soul Society and Hueco Mundo, the home of Hollows and Arrancar. In retaliation, Ichigo and his friends must fight alongside old allies and enemies alike to end Yhwach's campaign of carnage before the world itself comes to an end.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1956/126621.webp?s=26ab334183cbf99ac68db5b8caf0102f",
      "type": "TV"
    },
    {
      "_id": "9253",
      "title": "Steins;Gate",
      "alternativeTitles": ["STEINS;GATE", "Steins;Gate"],
      "ranking": 3,
      "genres": ["Drama", "Sci-Fi", "Suspense"],
      "episodes": 24,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1935/127974.webp",
      "link": "https://myanimelist.net/anime/9253/Steins_Gate",
      "status": "Finished Airing",
      "synopsis":
          "Eccentric scientist Rintarou Okabe has a never-ending thirst for scientific exploration. Together with his ditzy but well-meaning friend Mayuri Shiina and his roommate Itaru Hashida, Rintarou founds the Future Gadget Laboratory in the hopes of creating technological innovations that baffle the human psyche. Despite claims of grandeur, the only notable \"gadget\" the trio have created is a microwave that has the mystifying power to turn bananas into green goo.\n\nHowever, when Rintarou decides to attend neuroscientist Kurisu Makise's conference on time travel, he experiences a series of strange events that lead him to believe that there is more to the \"Phone Microwave\" gadget than meets the eye. Apparently able to send text messages into the past using the microwave, Rintarou dabbles further with the \"time machine,\" attracting the ire and attention of the mysterious organization SERN.\n\nDue to the novel discovery, Rintarou and his friends find themselves in an ever-present danger. As he works to mitigate the damage his invention has caused to the timeline, he is not only fighting a battle to save his loved ones, but also one against his degrading sanity.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1935/127974.webp?s=983a0b526d944e68a933acca0a7db043",
      "type": "TV"
    },
    {
      "_id": "28977",
      "title": "Gintama°",
      "alternativeTitles": [
        "Gintama' (2015)",
        "銀魂°",
        "Gintama Season 4",
        "Gintama Season 4",
        "Gintama Temporada 4",
        "Gintama Saison 4"
      ],
      "ranking": 4,
      "genres": ["Action", "Comedy", "Sci-Fi"],
      "episodes": 51,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/3/72078.webp",
      "link": "https://myanimelist.net/anime/28977/Gintama%C2%B0",
      "status": "Finished Airing",
      "synopsis":
          "Gintoki, Shinpachi, and Kagura return as the fun-loving but broke members of the Yorozuya team! Living in an alternate-reality Edo, where swords are prohibited and alien overlords have conquered Japan, they try to thrive on doing whatever work they can get their hands on. However, Shinpachi and Kagura still haven't been paid... Does Gin-chan really spend all that cash playing pachinko?\n\nMeanwhile, when Gintoki drunkenly staggers home one night, an alien spaceship crashes nearby. A fatally injured crew member emerges from the ship and gives Gintoki a strange, clock-shaped device, warning him that it is incredibly powerful and must be safeguarded. Mistaking it for his alarm clock, Gintoki proceeds to smash the device the next morning and suddenly discovers that the world outside his apartment has come to a standstill. With Kagura and Shinpachi at his side, he sets off to get the device fixed; though, as usual, nothing is ever that simple for the Yorozuya team.\n\nFilled with tongue-in-cheek humor and moments of heartfelt emotion, Gintama's fourth season finds Gintoki and his friends facing both their most hilarious misadventures and most dangerous crises yet.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/3/72078.webp?s=3c2861f116759641d5bffb941a86ffab",
      "type": "TV"
    },
    {
      "_id": "43608",
      "title": "Kaguya-sama wa Kokurasetai: Ultra Romantic",
      "alternativeTitles": [
        "Kaguya-sama wa Kokurasetai: Tensai-tachi no Renai Zunousen 3rd Season",
        "Kaguya-sama: Love is War Season 3rd Season",
        "かぐや様は告らせたい-ウルトラロマンティック-",
        "Kaguya-sama: Love is War - Ultra Romantic"
      ],
      "ranking": 5,
      "genres": ["Comedy", "Romance"],
      "episodes": 13,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1160/122627.webp",
      "link":
          "https://myanimelist.net/anime/43608/Kaguya-sama_wa_Kokurasetai__Ultra_Romantic",
      "status": "Finished Airing",
      "synopsis":
          "The elite members of Shuchiin Academy's student council continue their competitive day-to-day antics. Council president Miyuki Shirogane clashes daily against vice-president Kaguya Shinomiya, each fighting tooth and nail to trick the other into confessing their romantic love. Kaguya struggles within the strict confines of her wealthy, uptight family, rebelling against her cold default demeanor as she warms to Shirogane and the rest of her friends.\n\nMeanwhile, council treasurer Yuu Ishigami suffers under the weight of his hopeless crush on Tsubame Koyasu, a popular upperclassman who helps to instill a new confidence in him. Miko Iino, the newest student council member, grows closer to the rule-breaking Ishigami while striving to overcome her own authoritarian moral code.\n\nAs love further blooms at Shuchiin Academy, the student council officers drag their outsider friends into increasingly comedic conflicts.\n\n[Written by MAL Rewrite]\n",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1160/122627.webp?s=46750589d84c99f93ce6fec83d691766",
      "type": "TV"
    },
    {
      "_id": "38524",
      "title": "Shingeki no Kyojin Season 3 Part 2",
      "alternativeTitles": [
        "進撃の巨人 Season3 Part.2",
        "Attack on Titan Season 3 Part 2",
        "Attack on Titan Staffel 3 Teil 2",
        "Ataque a los Titanes Temporada 3 Parte 2",
        "L'Attaque des Titans Saison 3 Partie 2"
      ],
      "ranking": 6,
      "genres": ["Action", "Drama"],
      "episodes": 10,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1517/100633.webp",
      "link":
          "https://myanimelist.net/anime/38524/Shingeki_no_Kyojin_Season_3_Part_2",
      "status": "Finished Airing",
      "synopsis":
          "Seeking to restore humanity's diminishing hope, the Survey Corps embark on a mission to retake Wall Maria, where the battle against the merciless \"Titans\" takes the stage once again.\n\nReturning to the tattered Shiganshina District that was once his home, Eren Yeager and the Corps find the town oddly unoccupied by Titans. Even after the outer gate is plugged, they strangely encounter no opposition. The mission progresses smoothly until Armin Arlert, highly suspicious of the enemy's absence, discovers distressing signs of a potential scheme against them. \n\nShingeki no Kyojin Season 3 Part 2 follows Eren as he vows to take back everything that was once his. Alongside him, the Survey Corps strive—through countless sacrifices—to carve a path towards victory and uncover the secrets locked away in the Yeager family's basement.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1517/100633.webp?s=8ec097bf576c27c478e96097a85a6232",
      "type": "TV"
    },
    {
      "_id": "51535",
      "title": "Shingeki no Kyojin: The Final Season - Kanketsu-hen",
      "alternativeTitles": [
        "Shingeki no Kyojin: The Final Season Part 3",
        "Shingeki no Kyojin Season 4",
        "Attack on Titan Season 4",
        "進撃の巨人 The Final Season完結編",
        "Attack on Titan: The Final Season - Final Chapters"
      ],
      "ranking": 7,
      "genres": ["Action", "Drama", "Suspense"],
      "episodes": 2,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1279/131078.webp",
      "link":
          "https://myanimelist.net/anime/51535/Shingeki_no_Kyojin__The_Final_Season_-_Kanketsu-hen",
      "status": "Currently Airing",
      "synopsis":
          "In the wake of Eren Yeager's cataclysmic actions, his friends and former enemies form an alliance against his genocidal rampage. Though once bitter foes, Armin Arlert, Mikasa Ackerman, and the remaining members of the Scout Regiment join forces with Reiner Braun and the survivors of the Marleyan military. Their meager united front sets out on a mission to stop Eren's wrath and—if possible—save their old comrade in the process.\n\nAs Eren pushes forward at any cost, he battles his own internal turmoil. Although he feels immense remorse over his horrific invasion, Eren believes he harbors noble intentions: he believes the path ahead is the only way to save his friends and, to a greater extent, his people.\n\nThe opposing battalions spiral toward an inevitable final clash that may claim the lives of millions. Though they face an army of monsters beyond anything they could have previously imagined, Mikasa, Armin, and their allies stand brave in the face of certain doom.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1279/131078.webp?s=3651cb648c9f6defdb8a1f90ce43ed96",
      "type": "Special"
    },
    {
      "_id": "9969",
      "title": "Gintama'",
      "alternativeTitles": [
        "Gintama (2011)",
        "銀魂'",
        "Gintama Season 2",
        "Gintama Staffel 2",
        "Gintama Temporada 2",
        "Gintama Saison 2"
      ],
      "ranking": 8,
      "genres": ["Action", "Comedy", "Sci-Fi"],
      "episodes": 51,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/4/50361.webp",
      "link": "https://myanimelist.net/anime/9969/Gintama",
      "status": "Finished Airing",
      "synopsis":
          "After a one-year hiatus, Shinpachi Shimura returns to Edo, only to stumble upon a shocking surprise: Gintoki and Kagura, his fellow Yorozuya members, have become completely different characters! Fleeing from the Yorozuya headquarters in confusion, Shinpachi finds that all the denizens of Edo have undergone impossibly extreme changes, in both appearance and personality. Most unbelievably, his sister Otae has married the Shinsengumi chief and shameless stalker Isao Kondou and is pregnant with their first child.\n\nBewildered, Shinpachi agrees to join the Shinsengumi at Otae and Kondou's request and finds even more startling transformations afoot both in and out of the ranks of the the organization. However, discovering that Vice Chief Toushirou Hijikata has remained unchanged, Shinpachi and his unlikely Shinsengumi ally set out to return the city of Edo to how they remember it.\n\nWith even more dirty jokes, tongue-in-cheek parodies, and shameless references, Gintama' follows the Yorozuya team through more of their misadventures in the vibrant, alien-filled world of Edo.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/4/50361.webp?s=125342fa40b162d49da3f052ea2f34b3",
      "type": "TV"
    },
    {
      "_id": "39486",
      "title": "Gintama: The Final",
      "alternativeTitles": ["銀魂 THE FINAL", "Gintama: The Very Final", "N/A"],
      "ranking": 9,
      "genres": ["Action", "Comedy", "Drama", "Sci-Fi"],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1245/116760.webp",
      "link": "https://myanimelist.net/anime/39486/Gintama__The_Final",
      "status": "Finished Airing",
      "synopsis":
          "Two years have passed following the Tendoshuu's invasion of the O-Edo Central Terminal. Since then, the Yorozuya have gone their separate ways. Foreseeing Utsuro's return, Gintoki Sakata begins surveying Earth's ley lines for traces of the other man's Altana. After an encounter with the remnants of the Tendoshuu—who continue to press on in search of immortality—Gintoki returns to Edo.\n\nLater, the regrouped Shinsengumi and Yorozuya begin an attack on the occupied Central Terminal. With the Altana harvested by the wreckage of the Tendoshuu's ship in danger of detonating, the Yorozuya and their allies fight their enemies while the safety of Edo—and the rest of the world—hangs in the balance. Fulfilling the wishes of their teacher, Shouyou Yoshida's former students unite and relive their pasts one final time in an attempt to save their futures.\n\n[Written by MAL Rewrite]\n",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1245/116760.webp?s=f51738ce5277b7dbc661e5afe900f684",
      "type": "Movie"
    },
    {
      "_id": "11061",
      "title": "Hunter x Hunter (2011)",
      "alternativeTitles": [
        "HxH (2011)",
        "HUNTER×HUNTER（ハンター×ハンター）",
        "Hunter x Hunter",
        "Hunter x Hunter",
        "Hunter x Hunter",
        "Hunter X Hunter"
      ],
      "ranking": 10,
      "genres": ["Action", "Adventure", "Fantasy"],
      "episodes": 148,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1337/99013.webp",
      "link": "https://myanimelist.net/anime/11061/Hunter_x_Hunter_2011",
      "status": "Finished Airing",
      "synopsis":
          "Hunters devote themselves to accomplishing hazardous tasks, all from traversing the world's uncharted territories to locating rare items and monsters. Before becoming a Hunter, one must pass the Hunter Examination—a high-risk selection process in which most applicants end up handicapped or worse, deceased.\n\nAmbitious participants who challenge the notorious exam carry their own reason. What drives 12-year-old Gon Freecss is finding Ging, his father and a Hunter himself. Believing that he will meet his father by becoming a Hunter, Gon takes the first step to walk the same path.\n\nDuring the Hunter Examination, Gon befriends the medical student Leorio Paladiknight, the vindictive Kurapika, and ex-assassin Killua Zoldyck. While their motives vastly differ from each other, they band together for a common goal and begin to venture into a perilous world.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1337/99013.webp?s=e05aa402158661b59d14b9da6e917559",
      "type": "TV"
    },
    {
      "_id": "6325",
      "title": "Naruto: Shippuuden Movie 3 - Hi no Ishi wo Tsugu Mono",
      "alternativeTitles": [
        "Naruto Shippuuden: Gekijouban Naruto Shippuuden: Hi no Ishi o Tsugu Mono",
        "Naruto Shippuuden Movie 3",
        "Naruto Movie 6",
        "Naruto Shippuuden 3: Inheritors of Will of Fire",
        "ナルト- 疾風伝 火の意志を継ぐ者",
        "Naruto Shippuden the Movie 3: The Will of Fire",
        "Naruto Shippuden Film 3: Die Erben des Willens des Feuers",
        "Naruto Shippuden Película 3: The Will of Fire",
        "Naruto Shippuden Film 3: La Flamme de la Volonté"
      ],
      "ranking": 2373,
      "genres": ["Action", "Adventure", "Fantasy"],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1493/116732.webp",
      "link":
          "https://myanimelist.net/anime/6325/Naruto__Shippuuden_Movie_3_-_Hi_no_Ishi_wo_Tsugu_Mono",
      "status": "Finished Airing",
      "synopsis":
          "After being sent to investigate the alarming disappearance of four bloodline limit-wielding ninjas from different countries, Kakashi Hatake, Naruto Uzumaki, Sakura Haruno, and Sai successfully discover their whereabouts and inform the Hokage. Unexpectedly, Tsunade's further arrangements fall apart when Hiruko—the mastermind behind the incident and also a former Konohagakure ninja obsessed with power—appears to announce that he has absorbed the missing ninjas' unique abilities. On the verge of becoming invincible, he seeks one more bloodline limit before starting an all-out war to take over the world.\n\nAs Konohagakure's past connections with Hiruko raise suspicions among the nations about its involvement in the affair, Tsunade receives an ultimatum to solve the crisis. Left with no other choice, she decides to follow Kakashi's lead after he presents a daring yet salutary scheme—a proposal that could send him to certain death. However, Naruto opposes such a plan! Despite the Hokage's decision, he is determined to save his teacher's life, even if it means fighting friend and foe alike.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1493/116732.webp?s=dc3c14a676a0535d2087461c7715e63f",
      "type": "Movie"
    },
    {
      "_id": "936",
      "title":
          "Naruto Movie 2: Dai Gekitotsu! Maboroshi no Chiteiiseki Dattebayo!",
      "alternativeTitles": [
        "Naruto THE Movie vol.2",
        "Naruto Movie 2",
        "Gekijouban Naruto",
        "劇場版　NARUTO　大激突！幻の地底遺跡だってばよ",
        "Naruto the Movie 2: Legend of the Stone of Gelel",
        "Naruto Film 2: Die Legende des Steins von Gelel",
        "Naruto Película 2: Las Ruinas Ilusorias en lo Profundo de la Tierra",
        "Naruto Film 2: La légende de la Pierre de Guelel"
      ],
      "ranking": 4479,
      "genres": ["Action", "Adventure", "Fantasy"],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1114/134485.webp",
      "link":
          "https://myanimelist.net/anime/936/Naruto_Movie_2__Dai_Gekitotsu_Maboroshi_no_Chiteiiseki_Dattebayo",
      "status": "Finished Airing",
      "synopsis":
          "In a tumultuous effort, the Sunagakure ninjas attempt to repel an unforeseen invasion of mysterious armored warriors on the Land of Wind. Shortly afterwards, the same armored troops led by Temujin—a skilled knight of impressive power—ambush Naruto Uzumaki, Sakura Haruno, and Shikamaru Nara, who are on a mission to recover a lost ferret. Naruto and Temujin engage in a fierce fight that ends with both of them falling off a cliff.\n\nTaken aback by their friend's sudden misfortune, Sakura and Shikamaru witness yet another alarming development: a massive moving structure appears out of nowhere, ravaging any trees and rocks in its path. While Sakura sets off to find Naruto, Shikamaru infiltrates the imposing fortress in hopes of learning more about the critical situation. \n\nEntangled in a relentless conflict, the Konohagakure ninjas join forces with their Sunagakure counterparts to defeat the common enemy. However, amidst the turmoil, a clash between two different visions of an ideal world emerges.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1114/134485.webp?s=6ac81411f3f91aea7fa5f6bcb470bc10",
      "type": "Movie"
    },
    {
      "_id": "16870",
      "title": "The Last: Naruto the Movie",
      "alternativeTitles": [
        "Naruto Movie 10: Naruto the Movie: The Last",
        "Naruto: Shippuuden Movie 7 - The Last",
        "THE LAST NARUTO THE MOVIE",
        "Naruto Shippuden the Movie 7: The Last",
        "Naruto Film 7: The Last",
        "Naruto Película 7: The Last",
        "Naruto Film 7: The Last"
      ],
      "ranking": 944,
      "genres": ["Action", "Adventure", "Fantasy", "Romance"],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1491/134498.webp",
      "link": "https://myanimelist.net/anime/16870/The_Last__Naruto_the_Movie",
      "status": "Finished Airing",
      "synopsis":
          "Two years have passed since the end of the Fourth Great Ninja War. Konohagakure has remained in a state of peace and harmony—until Sixth Hokage Kakashi Hatake notices the moon is dangerously approaching the Earth, posing the threat of planetary ruin.\n\nAmidst the grave ordeal, the Konoha is invaded by a new evil, Toneri Oosutuski, who suddenly abducts Hinata Hyuuga's little sister Hanabi. Kakashi dispatches a skilled ninja team comprised of Naruto Uzumaki, Sakura Haruno, Shikamaru Nara, Sai, and Hinata in an effort to rescue Hanabi from the diabolical clutches of Toneri. However, during their mission, the team faces several obstacles that challenge them, foiling their efforts.\n\nWith her abduction, the relationships the team share with one another are tested, and with the world reaching the brink of destruction, they must race against time to ensure the safety of their planet. Meanwhile, as the battle ensues, Naruto is driven to fight for something greater than he has ever imagined—love.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1491/134498.webp?s=70f41bd09dddbf26e9124edfed2631d1",
      "type": "Movie"
    },
    {
      "_id": "2472",
      "title": "Naruto: Shippuuden Movie 1",
      "alternativeTitles": [
        "Naruto Shippuden Movie",
        "Naruto Movie 4",
        "Gekijouban Naruto Shippuuden",
        "劇場版NARUTO -ナルト- 疾風伝",
        "Naruto Shippuden the Movie 1",
        "Naruto Shippuden: Der Film",
        "Naruto Shippuden: La Película",
        "Naruto Shippuden Le Film: Un Funeste Présage"
      ],
      "ranking": 2579,
      "genres": ["Action", "Adventure", "Fantasy"],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1703/134493.webp",
      "link": "https://myanimelist.net/anime/2472/Naruto__Shippuuden_Movie_1",
      "status": "Finished Airing",
      "synopsis":
          "A group of ninja is planning to revive a powerful demon, and once its spirit is reunited with its body, the world will be destroyed. The only way to prevent this from happening is for Shion, a shrine maiden, to seal it away for good.\n\nNaruto Uzumaki is tasked to guard her, but one thing stops Shion from accepting his help: she also has the ability to predict death—and she has foreseen his demise approaching soon. In order to escape his fate, Naruto must stay away from Shion; however, undeterred, he chooses to challenge the prophecy in order to protect Shion and the world.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1703/134493.webp?s=41e3f50093f2f9ad4a34ac1619cca4ac",
      "type": "Movie"
    },
    {
      "_id": "28755",
      "title": "Boruto: Naruto the Movie",
      "alternativeTitles": [
        "Gekijouban Naruto (2015)",
        "BORUTO -NARUTO THE MOVIE-",
        "Boruto: Naruto the Movie",
        "Boruto: Naruto The Movie",
        "Boruto: Naruto La Película",
        "Boruto: Naruto Le Film"
      ],
      "ranking": 2084,
      "genres": ["Action", "Adventure", "Fantasy"],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/4/78280.webp",
      "link": "https://myanimelist.net/anime/28755/Boruto__Naruto_the_Movie",
      "status": "Finished Airing",
      "synopsis":
          "The spirited Boruto Uzumaki, son of Seventh Hokage Naruto, is a skilled ninja who possesses the same brashness and passion his father once had. However, the constant absence of his father, who is busy with his Hokage duties, puts a damper on Boruto's fire. Upon learning that his father will watch the aspiring ninjas who will participate in the upcoming Chunin exams, Boruto is driven to prove to him that he is worthy of his attention. In order to do so, he enlists the help of Naruto's childhood friend and rival, Sasuke Uchiha. \n\nThe Chunin exams begin and progress smoothly, until suddenly, the Konohagakure is attacked by a new foe that threatens the long-standing peace of the village. Now facing real danger, Naruto and his comrades must work together to protect the future of their cherished home and defeat the evil that terrorizes their world. As this battle ensues, Boruto comes to realize the struggles his father once experienced—and what it truly means to be a ninja.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/4/78280.webp?s=71576220e6555e1db01d098b60976da3",
      "type": "Movie"
    },
    {
      "_id": "1735",
      "title": "Naruto: Shippuuden",
      "alternativeTitles": [
        "Naruto Hurricane Chronicles",
        "-ナルト- 疾風伝",
        "Naruto Shippuden",
        "Naruto Shippuden",
        "Naruto Shippuden",
        "Naruto Shippuden"
      ],
      "ranking": 277,
      "genres": ["Action", "Adventure", "Fantasy"],
      "episodes": 500,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1565/111305.webp",
      "link": "https://myanimelist.net/anime/1735/Naruto__Shippuuden",
      "status": "Finished Airing",
      "synopsis":
          "It has been two and a half years since Naruto Uzumaki left Konohagakure, the Hidden Leaf Village, for intense training following events which fueled his desire to be stronger. Now Akatsuki, the mysterious organization of elite rogue ninja, is closing in on their grand plan which may threaten the safety of the entire shinobi world.\n \nAlthough Naruto is older and sinister events loom on the horizon, he has changed little in personality—still rambunctious and childish—though he is now far more confident and possesses an even greater determination to protect his friends and home. Come whatever may, Naruto will carry on with the fight for what is important to him, even at the expense of his own body, in the continuation of the saga about the boy who wishes to become Hokage.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1565/111305.webp?s=4dd8f8839f26ea747e4f0679a906f525",
      "type": "TV"
    },
    {
      "_id": "442",
      "title":
          "Naruto Movie 1: Dai Katsugeki!! Yuki Hime Shinobu Houjou Dattebayo!",
      "alternativeTitles": [
        "Naruto: Daikatsugeki! Yukihime Ninpocho Dattebayo!",
        "Naruto: It's the Snow Princess' Ninja Art Book!",
        "劇場版　NARUTO　大活劇！雪姫忍法帖だってばよ!!",
        "Naruto the Movie 1: Ninja Clash in the Land of Snow",
        "Naruto Der Film: Geheimmission im Land des Ewigen Schnees",
        "Naruto Le Film: Naruto et la Princesse des Neiges"
      ],
      "ranking": 3495,
      "genres": ["Action", "Adventure", "Fantasy"],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1231/134484.webp",
      "link":
          "https://myanimelist.net/anime/442/Naruto_Movie_1__Dai_Katsugeki_Yuki_Hime_Shinobu_Houjou_Dattebayo",
      "status": "Finished Airing",
      "synopsis":
          "Naruto Uzumaki and his squadmates, Sasuke Uchiha and Sakura Haruno, are sent on a mission to escort a movie crew on its way to film in the Land of Snow. They soon find out that they are accompanying a famous actress, Yukie Fujikaze, who persistently refuses to travel there, making the trip far more difficult than originally intended. After a surprising encounter with ninjas from the Land of Snow, Naruto discovers that there is more to Yukie than meets the eye.\n \nDai Katsugeki!! Yuki Hime Shinobu Houjou Dattebayo! follows the group as they attempt to overcome the obstacles in the Land of Snow and unveil Yukie's true purpose there as well.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1231/134484.webp?s=373f71b13606001ef155bbc092103d69",
      "type": "Movie"
    },
    {
      "_id": "4437",
      "title": "Naruto: Shippuuden Movie 2 - Kizuna",
      "alternativeTitles": [
        "Naruto Movie 5",
        "Naruto Shippuuden Movie 2",
        "Naruto Shippuuden: Bonds",
        "劇場版NARUTO-ナルト- 疾風伝 絆",
        "Naruto Shippuden the Movie 2: Bonds",
        "Naruto Shippuden Film 2 -Bonds-",
        "Naruto Shippuden Film 2 -Les Liens-"
      ],
      "ranking": 2612,
      "genres": ["Action", "Adventure", "Fantasy"],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1484/134494.webp",
      "link":
          "https://myanimelist.net/anime/4437/Naruto__Shippuuden_Movie_2_-_Kizuna",
      "status": "Finished Airing",
      "synopsis":
          "Unleashing a devastating surprise attack, flying ninjas from the Land of Sky are seeking revenge against their old enemy Konohagakure. Despite his eagerness to join the fight, Naruto Uzumaki is held up by Shinnou, a mysterious doctor who requires his assistance to save an injured person. While delivering the wounded man to the hospital, Naruto has an unexpected encounter with Amaru—a stormy youngster from a neighboring village desperately looking for Shinnou's help.\n\nMeanwhile, as the invaders withdraw to restore their forces, Tsunade seizes the opportunity to dispatch a small team including Sai, Shikamaru Nara, and Kakashi Hatake to strike down their base. Simultaneously, she commissions Naruto, Sakura Haruno, and Hinata Hyuuga to accompany Shinnou and Amaru on their journey. During this time, however, Orochimaru has his own schemes: he orders Sasuke Uchiha to bring him the doctor who specializes in the reincarnation technique.\n\nAs the escort group accidentally uncovers the sinister truth behind the attacks, tumultuous plans are set in motion. But even when confronted with an unforeseen reunion, Naruto does not waver in his endeavor to end the warfare and its disastrous effects.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1484/134494.webp?s=ad7453b921b0ccc42fafc7fa04382514",
      "type": "Movie"
    },
    {
      "_id": "2144",
      "title":
          "Naruto Movie 3: Dai Koufun! Mikazuki Jima no Animaru Panikku Dattebayo!",
      "alternativeTitles": [
        "Naruto Movie Volume 3",
        "Naruto: Gekijouban Naruto: Dai Koufun! Mikazuki-jima no Animal Panic Datte ba yo!",
        "劇場版 NARUTO -ナルト- 大興奮!みかづき島のアニマル騒動だってばよ",
        "Naruto the Movie 3: Guardians of the Crescent Moon Kingdom",
        "Naruto The Movie 3: Die Hüter des Sichelmondreichs",
        "Naruto Le Film 3: Mission Spéciale au Pays de la Lune"
      ],
      "ranking": 4302,
      "genres": ["Action", "Adventure", "Fantasy"],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1918/134487.webp",
      "link":
          "https://myanimelist.net/anime/2144/Naruto_Movie_3__Dai_Koufun_Mikazuki_Jima_no_Animaru_Panikku_Dattebayo",
      "status": "Finished Airing",
      "synopsis":
          "Led by Kakashi Hatake, Naruto Uzumaki, Sakura Haruno, and Rock Lee are tasked to escort the extravagant Prince Michiru Tsuki and his spoiled son Hikaru to the prosperous Land of Moon when the two return from a long trip around the world. As if guarding two whimsical high-ranked individuals was not challenging enough, the prince's reckless decision to acquire an entire circus during their journey—mainly to entertain Hikaru's wish of owning the saber-toothed tiger featured in the show—further propels the mission into disarray.\n\nJust as things are finally settling down, the arrival of Michiru's convoy at the Land of Moon is met with an unforeseen crisis—the greedy Chief Minister Shabadaba has taken over the country with the assistance of mysterious, powerful ninjas. While Kakashi's team relentlessly fights the enemy by any means necessary, the two princes are forced to confront a new outlook on life through adversity.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1918/134487.webp?s=a7d1945387d6a0210f3263439ff879cc",
      "type": "Movie"
    },
    {
      "_id": "8246",
      "title": "Naruto: Shippuuden Movie 4 - The Lost Tower",
      "alternativeTitles": [
        "Naruto Movie 7",
        "Gekijouban Naruto Shippuuden: The Lost Tower",
        "劇場版 NARUTO-ナルト-疾風伝 ザ・ロストタワー",
        "Naruto Shippuden the Movie 4: The Lost Tower",
        "Naruto Shippuden Film 4: The Lost Tower",
        "Naruto Shippuden Film 4: The Lost Tower"
      ],
      "ranking": 1997,
      "genres": ["Action", "Adventure", "Fantasy"],
      "episodes": 1,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1479/116734.webp",
      "link":
          "https://myanimelist.net/anime/8246/Naruto__Shippuuden_Movie_4_-_The_Lost_Tower",
      "status": "Finished Airing",
      "synopsis":
          "Led by Yamato, Naruto Uzumaki, Sakura Haruno, and Sai are assigned to capture Mukade, a rogue ninja who is pursuing the ancient chakra Ryuumyaku located underneath the Rouran ruins. While the Ryuumyaku has been sealed by the Fourth Hokage, the group fails to prevent Mukade from releasing its power. Consequently, a strong energy burst engulfs both Naruto and Yamato before they can escape.\n\nAs he awakens in a magnificent yet hostile kingdom, Naruto meets its young queen Saara and three Konohagakure ninjas on a top-secret mission. They reveal to him that he has time-traveled to Rouran 20 years into the past! To make matters worse, Mukade has already infiltrated the royal court, becoming the naive queen's most trusted minister under the alias Anrokuzan.\n\nJoining forces with the three ninjas, Naruto must protect Saara's life without fail to stop the villain's plans and return to the present.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1479/116734.webp?s=12984063a1f5f24d3dd5e542ce49a2a6",
      "type": "Movie"
    },
    {
      "_id": "38000",
      "title": "Kimetsu no Yaiba",
      "alternativeTitles": [
        "Blade of Demon Destruction",
        "鬼滅の刃",
        "Demon Slayer: Kimetsu no Yaiba",
        "Demon Slayer",
        "Guardianes De La Noche: Kimetsu no Yaiba",
        "Demon Slayer"
      ],
      "ranking": 125,
      "genres": ["Action", "Award Winning", "Fantasy"],
      "episodes": 26,
      "hasEpisode": true,
      "hasRanking": true,
      "image": "https://cdn.myanimelist.net/images/anime/1286/99889.webp",
      "link": "https://myanimelist.net/anime/38000/Kimetsu_no_Yaiba",
      "status": "Finished Airing",
      "synopsis":
          "Ever since the death of his father, the burden of supporting the family has fallen upon Tanjirou Kamado's shoulders. Though living impoverished on a remote mountain, the Kamado family are able to enjoy a relatively peaceful and happy life. One day, Tanjirou decides to go down to the local village to make a little money selling charcoal. On his way back, night falls, forcing Tanjirou to take shelter in the house of a strange man, who warns him of the existence of flesh-eating demons that lurk in the woods at night.\n\nWhen he finally arrives back home the next day, he is met with a horrifying sight—his whole family has been slaughtered. Worse still, the sole survivor is his sister Nezuko, who has been turned into a bloodthirsty demon. Consumed by rage and hatred, Tanjirou swears to avenge his family and stay by his only remaining sibling. Alongside the mysterious group calling themselves the Demon Slayer Corps, Tanjirou will do whatever it takes to slay the demons and protect the remnants of his beloved sister's humanity.\n\n[Written by MAL Rewrite]",
      "thumb":
          "https://cdn.myanimelist.net/r/50x70/images/anime/1286/99889.webp?s=5697462c0ee69a372e4abef4bec21983",
      "type": "TV"
    },
  ],
  "meta": {"page": 1, "size": 10, "totalData": 24727, "totalPage": 2473}
};
