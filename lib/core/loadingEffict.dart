import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class Skeleton extends StatelessWidget {
  final double width;
  final double height;
  ShapeBorder? shapeBorder;
  Skeleton.rectangular({required this.width, required this.height}) {
    shapeBorder =
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(5));
  }
  Skeleton.circular(
      {required this.width,
      required this.height,
      this.shapeBorder = const CircleBorder()});
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
        baseColor: Colors.grey.withOpacity(0.7),
        highlightColor: Colors.grey[200]!,
        child: Container(
          width: width,
          height: height,
          decoration:
              ShapeDecoration(shape: shapeBorder!, color: Colors.grey[100]),
        ));
  }
}
