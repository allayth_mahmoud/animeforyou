import 'package:flutter/material.dart';

TextStyle textStyle(
        {required double size,
        required Color color,
        required FontWeight weight}) =>
    TextStyle(
      fontSize: size,
      color: color,
      fontWeight: weight,
    );
