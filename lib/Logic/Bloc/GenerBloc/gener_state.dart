part of 'gener_bloc.dart';

abstract class GenerState extends Equatable {
  const GenerState();

  @override
  List<Object> get props => [];
}

class GenerInitial extends GenerState {}

class GenerLoaded extends GenerState {
  final List<String> gener;
  final String selectedid;

  GenerLoaded({required this.gener, this.selectedid = 'Award Winning'});
  @override
  List<Object> get props => [gener, selectedid];
}

class ErrorStateG extends GenerState {
  final String error;

  ErrorStateG({required this.error});
  @override
  List<Object> get props => [error];
}
