part of 'gener_bloc.dart';

abstract class GenerEvent extends Equatable {
  const GenerEvent();

  @override
  List<Object> get props => [];
}

class GetGener extends GenerEvent {}

class SelectGener extends GenerEvent {
  final String id;

  SelectGener({required this.id});
  @override
  List<Object> get props => [id];
}
