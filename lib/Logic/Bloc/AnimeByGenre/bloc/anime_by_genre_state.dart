// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'anime_by_genre_bloc.dart';

abstract class AnimeByGenreState extends Equatable {
  const AnimeByGenreState();

  @override
  List<Object> get props => [];
}

class AnimeByGenreInitial extends AnimeByGenreState {}

class AnimeByGenreLoaded extends AnimeByGenreState {
  final List<Animes> anime;
  AnimeByGenreLoaded({
    required this.anime,
  });
  @override
  List<Object> get props => [anime];
}
