part of 'search_bloc.dart';

abstract class SearchEvent extends Equatable {}

class GetSearchResult extends SearchEvent {
  GetSearchResult({required this.name});
  final String name;
  @override
  List<Object> get props => [name];
}
