import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../data/datasources/SearchFor.dart';
import '../../../data/models/AnimesModel.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final SearchServices searchServices;
  SearchBloc(this.searchServices) : super(SearchInitial()) {
    on<GetSearchResult>((event, emit) async {
      print("00");
      try {
        List<Animes> dataList = await searchServices.searchforAnime(event.name);

        emit(SearchListLoaded(dataList));
        print("01");
      } catch (e) {
        print("02");
        emit(ErrorState(e.toString()));
      }
    });
  }
}
