 import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/datasources/getMovieAnimes.dart';
import '../../../data/models/AnimesModel.dart';

part 'anime_event.dart';
part 'anime_state.dart';

class AnimeBloc extends Bloc<AnimeEvent, AnimeState> {
  final AnimeServices services;
  AnimeBloc({required this.services}) : super(AnimeInitial()) {
    on<GetMovieEvent>((event, emit) async {
      emit(AnimeInitial());
      await Future.delayed(Duration(seconds: 2));
      try {
        List<Animes> animes =
            await services.getAnimes(isGenre: false, genre: event.genre);

        emit(AnimeLoaded(animes));
      } catch (e) {
        emit(ErrorState(e.toString()));
      }
    });
    on<GetAnimesByGenreEvent>((event, emit) async {
      emit(AnimeInitial());
      try {
        List<Animes> animes =
            await services.getAnimes(isGenre: true, genre: event.genre);

        emit(AnimeLoaded(animes));
      } catch (e) {
        emit(ErrorState(e.toString()));
      }
    });
  }
}
