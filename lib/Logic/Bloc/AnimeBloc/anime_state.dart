part of 'anime_bloc.dart';

abstract class AnimeState extends Equatable {
  const AnimeState();

  @override
  List<Object> get props => [];
}

class AnimeInitial extends AnimeState {}

class AnimeLoaded extends AnimeState {
  final List<Animes> animes;

  const AnimeLoaded(this.animes);

  @override
  List<Object> get props => [animes];
}

class ErrorState extends AnimeState {
  final String error;

  const ErrorState(this.error);

  @override
  List<Object> get props => [error];
}
