part of 'anime_bloc.dart';

abstract class AnimeEvent extends Equatable {
  const AnimeEvent();

  @override
  List<Object> get props => [];
}

class GetMovieEvent extends AnimeEvent {
  final String genre;

  const GetMovieEvent({required this.genre});
  @override
  List<Object> get props => [genre];
}

class GetAnimesByGenreEvent extends AnimeEvent {
  final String genre;

  const GetAnimesByGenreEvent({required this.genre});
  @override
  List<Object> get props => [genre];
}
